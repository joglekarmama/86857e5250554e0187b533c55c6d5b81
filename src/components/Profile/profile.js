import React, { useState, Fragment } from 'react';
import config from 'visual-config-exposer';

import './profile.css';

const name = config.settings.profileName;
const description = config.settings.profileDesc;
const image = config.settings.profileImg;

const Profile = () => {
  return (
    <Fragment>
      <div className="profile_img-container">
        <img src={image} alt="profile_image" className="profile_image" />
      </div>
      <div className="profile_name">{name}</div>
      <div className="profile_desc">{description}</div>
    </Fragment>
  );
};

export default Profile;
